<!-- Name this issue "Week 2 - Mastering my stories" -->
Spend this week continuing to reflect on your conversations, especially those that are crucial.

For each conversation/situation, think about:

- What did I experience that made me think this?
- What would the other people involved have seen or experienced?
- Ask: 
   - What is my role?
   - Why would a reasonable, rational, decent person do this?
   - What can I do right now to move toward what I really want?

-----

- [ ] Identify 5 different conversations or situations and reflect on the items above.
- [ ] At the end of the week, share what you've noticed about your conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"