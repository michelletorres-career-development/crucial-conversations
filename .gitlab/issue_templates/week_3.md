<!-- Name this issue "Week 3 - Start with heart" -->
This week, start using the "Start with Heart" skill by preparing for conversations beforehand.

1. What do my actions communicate about my intent?
1. What is my intent for myself, others, the relationship, the organization?

-----

- [ ] Prepare for 5 different conversations by asking yourself the questions above.
- [ ] Start 5 conversations by sharing your good intent when initializing the conversation. If the conversation is ongoing, apologize and restate your intent.
- [ ] At the end of the week, share what you've noticed about these conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"