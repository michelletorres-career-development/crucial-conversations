<!-- Week 5 - Make it safe -->
Spend this week focusing on making your conversations safe for others.

- [ ] Use contrasting (don't/do) in 5 conversations to fix a misunderstood intent
- [ ] At the end of the week, share what you've noticed about these conversations.

/label ~"status::todo"
/label ~"Crucial Conversations"