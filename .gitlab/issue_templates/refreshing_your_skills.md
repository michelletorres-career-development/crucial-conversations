<!-- Name this issue "Beyond - Refreshing your skills" -->
Set a reminder 3, 6, and 12 months from now.

- Look at the list of skills and list which you are still using often and which you have lost the habit on or need to work on.
- For each skill you need to practice, focus on each one in individual conversations. Log those conversations here.
- If you need extended practice, re-open the issue related to each skill and focus a full week on them once again.

/label ~"status::todo"
/label ~"Crucial Conversations"